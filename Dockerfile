FROM python:3.7

WORKDIR /app

RUN pip3 install redis flask
COPY app.py .

RUN useradd -m -u 1111 proton
USER 1111

ENTRYPOINT ["python3", "app.py"]
